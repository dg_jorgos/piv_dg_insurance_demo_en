<?php
/**
 * Common Controller Class
 */
class Controller
{
    /**
     * Function to make to a cURL request to a URL
     *
     * @param string $url The URL to query
     * @param string $username The username to authenticate the request with
     * @param string $password The password to authenticate the request with
     *
     * @return the URL output
     */
    protected static function getUrl($url, $username = "", $password = "")
    {
        $ch = curl_init();

        $logPath = App::getSetting("piv_log_dir") . "/" . App::getServiceName() . "/apis.log";

        if (!file_exists(dirname($logPath))) {
            mkdir(dirname($logPath), 0755, true);
        }

        file_put_contents($logPath, "[" . date("Y-m-d H:i:s") . "] Calling $url", FILE_APPEND);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //Authenticate the request if necessary
        if (!empty($username) && !empty($password)) {
            curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
        }

        $output = curl_exec($ch);

        curl_close($ch);

        return $output;
    }
}